I used this ansible construct to play around with kubernetes.

The script installs everything on the local machine needed to create the kubernete cluster. Afterwards, cloud nodes in the Hetzner cloud are created and the kubernetes cluster is rolled out on these nodes.

Many things in this repository are deprecated today. This repository is only used for reference.
