#!/bin/sh
LANG=C

PLAY_NAME='ansible/infrastructure.yml'

. assets/init.sh

# run playbook
echo "\e[33m### - create hcloud.\e[0m"
rm -f $(echo "$PLAY_NAME" | sed 's/yml$/retry/g')
ansible-playbook -M ansible/library -i ansible/inventories/localhost "$PLAY_NAME"
