#!/bin/sh
LANG=C

export HCLOUD_TOKEN=***REMOVED***

# install needed debian packages
if ! dpkg -s virtualenv > /dev/null; then
    echo "\e[33m### - virtualenv needs to be installed.\e[0m"
    sudo apt install virtualenv
else
    echo "\e[32m### - virtualenv is installed.\e[0m"
fi

# make sure we have a virtualenv and we actually use it
if [ ! -e env/bin/activate ]; then
    echo "\e[33m### - virtualenv is not initialized. Initializing now.\e[0m"
    virtualenv -p python3 env
else
    echo "\e[32m### - virtualenv is initialized\e[0m"
fi
if [ -n "$VIRTUAL_ENV" ] && [ "$VIRTUAL_ENV" = "$(realpath env)" ]; then
    echo "\e[32m### - activating virtualenv\e[0m"
else
    echo "\e[33m### - activating virtualenv\e[0m"
    . env/bin/activate
fi

# install needed python modules
if ! pip list 2> /dev/null | grep -q ansible; then
    echo "\e[33m### - install needed python modules.\e[0m"
    pip install -r requirements.txt
else
    echo "\e[32m### - all needed python modules present.\e[0m"
fi

# install hcloud modules for ansible
if [ ! -x ansible/library/hcloud_floating_ip ] || [ ! -x ansible/library/hcloud_inventory ] || \
       [ ! -x ansible/library/hcloud_server ] || [ ! -x ansible/library/hcloud_ssh_key ]; then
    echo "\e[33m### - install hcloud ansible modules.\e[0m"
    mkdir -p ansible/library
    wget https://github.com/thetechnick/hcloud-ansible/releases/download/v0.3.0/hcloud-ansible_v0.3.0_linux_amd64.zip -O hcloud_modules.zip
    unzip hcloud_modules.zip -d ansible/library
    ln -s $(realpath ansible/library/hcloud_inventory) $(realpath ansible/inventories/mixed)
    rm hcloud_modules.zip ansible/library/README.md ansible/library/LICENSE
else
    echo "\e[32m### - hcloud ansible modules present.\e[0m"
fi
