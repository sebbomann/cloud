#!/bin/sh
LANG=C

PLAY_NAME='ansible/nihilate.yml'

. assets/init.sh

read -p "Are you sure you want to nihilate the hcloud? Then enter 'yes please' " -r REPLY
echo    # (optional) move to a new line
if [ "$REPLY" = "yes please" ]; then
    echo "\e[32m### - Nihilating hcloud\e[0m"
    rm -f $(echo "$PLAY_NAME" | sed 's/yml$/retry/g')
    ansible-playbook -M ansible/library -i ansible/inventories/mixed "$PLAY_NAME"
else
    echo "\e[31m### - Bye..\e[0m"
fi
